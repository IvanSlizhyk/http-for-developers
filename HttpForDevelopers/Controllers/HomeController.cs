﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HttpForDevelopers.Filters;

namespace HttpForDevelopers.Controllers
{
    public class HomeController : Controller
    {
        [CompressFilter]
        [EnableETag]
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        [CompressFilter]
        [EnableETag]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [CompressFilter]
        [EnableETag]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
